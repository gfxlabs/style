# gfx golang style guide (rev 0.0.2)

- [gfx golang style guide](#gfx-golang-style-guide)
- [words](#words)
- [goals](#goals)
- [good habits](#good-habits)
  * [pass loop variables to lambda if lambda is a gorouting](#pass-loop-variables-to-lambda-if-lambda-is-a-gorouting)
  * [avoid goroutine](#avoid-goroutine)
  * [handle &| pass up](#handle----pass-up)
  * [struct first, interface after](#struct-first--interface-after)
- [formatting specific](#formatting-specific)
  * [gofmt](#gofmt)
  * [general casing](#general-casing)
  * [short names](#short-names)
- [code rules](#code-rules)
  * [interface validation](#interface-validation)
  * [pointer to interface](#pointer-to-interface)
  * [method receivers](#method-receivers)
  * [enums](#enums)
  * [use defer](#use-defer)
  * [prefer not nesting](#prefer-not-nesting)
  * [avoid else if possible](#avoid-else-if-possible)
  * [avoid embedding structs](#avoid-embedding-structs)
  * [mutex within struct](#mutex-within-struct)
  * [mutex initialization](#mutex-initialization)
  * [package time](#package-time)
  * [panic in main (and only main)](#panic-in-main--and-only-main-)
  * [always check casts (and map access)](#always-check-casts--and-map-access-)
  * [nil is a slice!](#nil-is-a-slice-)
  * [initialize references to structs, not structs themselves](#initialize-references-to-structs--not-structs-themselves)
  * [json encoding](#json-encoding)
  * [unsafe](#unsafe)
- [files & directory structure](#files---directory-structure)
  * [one exported struct per file](#one-exported-struct-per-file)
  * [file structure](#file-structure)

# words

- style: style refers not only to formatting. If you would like, replace the word "style" with "readability" 
- variable: a variable refers to anything (function, struct, int, etc)
- newfunc: a function that returns a pointer to a struct, meant to initialize struct
- visible/visibility: exported or internal or filescope
    - exported: a varaible that is exported from the package (most visible)
    - internal: a variable that is not exported from the package (middle visible)
    - filescope: a variable that is not to be used outside the file it is declared (least visible)

# goals

this document should

1. help you when you are confused on what to do
2. make it easier for you to write code
3. make it easier for others to read your code
4. make it easy for you to add to your code down the line

this document is not

1. a list of rules you must follow
2. a guide to optimize the performance of your program
3. something that should slow down your workflow


take a [worse is better](http://doc.cat-v.org/programming/worse_is_better) approach. code first, format later. (but do format)


# good habits

highly reccomend reading this section, if any

## pass loop variables to lambda if lambda is a gorouting

**do**
```go
for i:=0; i < n; i++ {
    go func(num int){
        log.Println(num) 
    }(i)
}
```
**don't** - this will in fact not work. it won't do what you think it does.
```go
for i:=0; i < n; i++ {
    go func(){
        log.Println(i) 
    }()
}
```

## avoid goroutine

if you can avoid using a goroutine, avoid it. you should use a goroutine when you need multiple things to run at once. Do not use a goroutine to pause calculation in one function and perform calculation in another.

## handle &| pass up

handle and pass up errors. only negate _ with err when

1. you are 100% sure that the code will not panic
2. you are testing/developing

otherwise, err MUST be either passed up as a return of the function, or gracefully handled to avoid panic

## struct first, interface after

when creating a new data structure, create the struct first, then create an interface using that working struct.



# formatting specific

## gofmt

gofmt is the built in go code formatter. All code should be linted with gofmt. This is typically automatically done by the IDE, but if you don't, the command is `go fmt`

all styles outlined by this document, therefore, should be considered valid by default gofmt. If gofmt changes rules, the gofmt output shall overrule any standards set here

## general casing

go uses the case of the first letter to determine whether or not a function should be exported.
the go style guide states that one should use "lowerCamelCase" and "UpperCamelCase"

we extend this by supporting uses for lower_snake_case and Upper_Snake_Case.

**lower_snake_case** should be used for variables that are not used outside of the file they are declared in (private within file)

**lower_snake_case** should be used for struct fields that are not accessed by other structs (private within struct) 

**Upper_Snake_Case** should be used for enums and enums only

**UpperSnakeCase** should be used for exported variables

**lowerSnakeCase** should be used for non-exported variables 

while these are loose guidelines, you should attempt to enforce them the best that you can.

## short names

prefer single character variables for private struct fields & temporary variables used within functions


# code rules

## interface validation

validate interfaces using
```go
var _ Interface = (*Struct)(nil)
```

this will err on compile time if "Struct" does not implement "Interface"

## pointer to interface

you should almost never need a pointer to an interface

interfaces themselves are actually a pointer to the underlying implementation

so a pointer to an interface is a pointer to a pointer

there's no point. (get it)

## method receivers

receivers should bind to the pointer, not the struct value.

**do**
```go
func (S *Struct) Read() string {
  return s.data
}
```

**not**
```go
func (S Struct) Read() string {
  return s.data
}
```

receivers bind should be a capital single letter
**do**
```go
func (S *Struct) Read() string {
  return s.data
}
```

**not**
```go
func (s *Struct) Read() string {
  return s.data
}
```

## enums

enums should start at 1 (unless it conflicts with the actual code, ala, you want 0 to be a default)

enums are 0 by default, so this way one can use 0 to figure out if the enum is invalid or not


## use defer

use defer to free your locks, close your io, etc, etc. This will make it so that you won't forget to release the lock (since you will do it on return)

## prefer not nesting

prefer to not nest your errors.

**do**
```go
a, err := func()
if err != nil {
    return err
}
b, err := func2(a)
if err != nil {
    return err
}
```

**not**
```go
a, err := func()
if err == nil {
    b, err := func2(a)
    if err != nil {
        return err
    }
}else{
   return err 
}
```

## avoid else if possible

common places to avoid if else
**do**
```go
a := 10
if b == 10 {
    a = 30
}
```

**not**
```go
var a int
if b == 10 {
    a = 30
} else{
    a = 10
}
```
another one
**do**
```go
if b == 10 {
    return 5
}
return 10
```

**not**
```go
if b == 10 {
    return 5
}else {
   return 10 
}

```
## avoid embedding structs 

you can put one struct into another. this will make the parent inheret all the childs methods and fields

avoid this when possible, however, sometimes it is the only way. there is usually another way though.

In most cases, simply give the nested struct a name

**do**
```go
type Example struct {
    e Example2
}

type Example2 struct {
    field string
}
```

**not**
```go
type Example struct {
    Example2
}

type Example2 struct {
    field string
}
```

## mutex within struct 

when you put a mutex within a struct, you do not need a pointer

**do**:
```go 
type Example struct {
    protected string
    
    sync.Mutex
}
```

**not**
```go 
type Example struct {
    protected string
    
    *sync.Mutex
}
```

since you will only access the mutex where you may access example, you don't need to put a pointer to it. You can think of it as just adding a few fields to the struct. (that is what you are doing)

## mutex initialization

the "zero value" of a mutex is valid. you never need to do sync.Mutex{} or new(sync.Mutex)

never 

never ever ever!


## package time

use the package time whenever you are dealing with dates, times, or durations

## panic in main (and only main)

code should NEVER panic unless it is in the main() function, or at most, within main.go

## always check casts (and map access)

whenever you cast an interface to a struct, remember to always check "ok", else risk panic

**do**
```go
t, ok := i.(string)
if !ok {
  // handle error
}
```
**not**
```go
t := i.(string)
```

same thing with map access
**do**
```go
t, ok := m[key]
if !ok {
  // handle error
}
```
**not**
```go
t := m[k]
```

## nil is a slice!

nil is a slice of length 0. This means that if your function returns a slice, you should return nil instead of []type{}

if you are reading return from a function that returns a slice, check if it is empty through len 
**do**
```go
len(slice) == 0
```
**not**
```go
slice==nil
```

## initialize references to structs, not structs themselves

when creating a struct, always create a pointer to it.

if you need the value, grab the value of that pointer and pass it.

this ensures that you won't make accidental copies.

## json encoding

when creating a struct that will be consumed by a web application (out of go)
json marshal should be in lower_snake_case. use [json2go](https://penguin.roma-tor.dgfx.wtf/) when in doubt.

if something is in all caps, it should be preserved as such

example: 

```go 
type Data struct {
    FieldOne string `json:"field_one"`
    FieldTWO string `json:"field_TWO"`
    Word string `json:"word"`
}
```

## unsafe

**rule:**
do not use package unsafe. If you feel like you need to use it, you probably don't. Talk to others before using unsafe

package "unsafe" offers low level access to go primatives, such as direct pointer/memory access

**issues**:
 - unspecified behavior
 - difficult to figure out what it do
 - non portable
 - not backwards compatible

don't use it

# files & directory structure

files should be in lower_camel_case
test cases should have name that reflects the file they are testing, appended with _test

example: test case for `magic_function.go` should be `magic_function_test.go`

if a file exports a struct, the file should be named the lower_camel_case name of that struct 

## one exported struct per file

a file should never export more than one struct. if you find yourself doing this, either unexport something that doesn't need to be exported, or create a new file


## file structure

In general, a file should be laid out in this order

1. package declaration
2. imports
    3. standard libs
    4. within-package deps
    5. external deps
6. interface checks    
7. global variables and/or enums
8. structs, in order of most-least visible
9. methods for structs, in order of most-least visible, groups by parent
10. non struct-bound methods, from most to least visible


```go
package example

imports(
    "math"  <- standard libs
    "fmt"
    // empty line
    "common/something" <- internal deps
    "common/database/parser"
    // empty line to separate 
    "github.com/a/external_dependency" <- external deps
)

struct Example{}

struct internalExample{}

struct file_only_example{}


func NewExample(args...) *Example {}

func (S *Example) ExportedStructMethod() {}

func (S *Example) internalStructMethod() {}

func (S *Example) file_only_method() {}
// note two lines to separate the two

func [N]ewInternalExample(args...) *internalExample {}

func (S *internalExample) ExportedStructMethod() {}

func (S *internalExample) internalStructMethod() {}

func (S *internalExample) file_only_method() {}
// two lines again

func newFileOnlyExample(args...) *file_only_example {}

func (S *file_only_example) ExportedStructMethod() {}

func (S *file_only_example) internalStructMethod() {}

func (S *file_only_example) file_only_method() {}
//two lines again!

func RelatedExportedFunction() {}

func relatedHelperFunction() {}

func very_specific_helper_function() {}

```
Alternatively, you may put each struct above its newFunc.

example:

```go
package example

imports(
    "math"  <- standard libs
    "fmt"
    // empty line
    "common/something" <- internal deps
    "common/database/parser"
    // empty line to separate 
    "github.com/a/external_dependency" <- external deps
)

struct Example{}

func NewExample(args...) *Example {}

func (S *Example) ExportedStructMethod() {}


struct internalExample{}

func [N]ewInternalExample(args...) *internalExample {}

func (S *internalExample) ExportedStructMethod() {}
```
